<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Node;

use Illuminate\Support\Facades\Log;

/**
 * Create a node
 */
class CreateNode extends Command
{
	protected $node = null;

	/**
	 * Set the node to drive the command
	 *
	 * @param Client $client
	 * @param Node $node
	 */
	public function __construct(Client $client, Node $node)
	{
		parent::__construct($client);
		$this->node = $node;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$statement = "CREATE (n { ";

		$nodeProperties = $this->node->getProperties();
		foreach ($nodeProperties as $key => $property) {

			if (is_string($property))
				$statement .= "{$key}: \"{$property}\", ";
			elseif (is_null($property))
				$statement .= "{$key}: null, ";
			elseif (is_bool($property))
				$statement .= "{$key}: ".($property ? "true" : "false").", ";
			elseif (is_array($property))
				$statement .= "{$key}: ".json_encode($property).", ";
			else
				$statement .= "{$key}: {$property}, ";

		}

		// construct the statement that will be in our statements array
		$statement = substr($statement, 0, -2);
		$statement .= ' }) RETURN n';

		// organize data for 4.0+ API format
		$statements = array('statement' => $statement);
		$data = array('statements' => array($statements));

		return $data;
		// return $this->node->getProperties() ?: null;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$host = $this->node->getClient()->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	protected function getHost()
	{
		return explode(":", explode("://", $this->node->getClient()->getTransport()->getEndpoint())[1])[0];
	}

	/**
	 * Post request to the transaction URL to persist to database (4.0+)
	 *
	 * @param array $data
	 * @return null
	 */
	protected function commitTransaction($data)
	{
		//! change the default argument to this in Transport.php's
		//! post function? this is what neo always expects on commit ig
		$trimmedCommitUrl = substr($data['commit'], 17);
		$dummyData = array('statements' => array());
		$this->client->getTransport()->post($trimmedCommitUrl, $dummyData);
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to create node', $code, $headers, $data);
		}

		$nodeId = $data['data'][0][0]['metadata']['id'];

		$this->node->setId($nodeId);

		if (isset($headers['Location'])) {
			$this->node->setCommitLocation(basename($headers['Location']));
		}

		$this->getEntityCache()->setCachedEntity($this->node);
		return true;
	}
}

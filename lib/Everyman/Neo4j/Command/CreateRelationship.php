<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Exception,
	Everyman\Neo4j\Relationship,
	Everyman\Neo4j\Node;

use Illuminate\Support\Facades\Log;

/**
 * Create a relationship
 */
class CreateRelationship extends Command
{
	protected $rel = null;

	/**
	 * Set the relationship to drive the command
	 *
	 * @param Client $client
	 * @param Relationship $rel
	 */
	public function __construct(Client $client, Relationship $rel)
	{
		parent::__construct($client);
		$this->rel = $rel;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$start = $this->rel->getStartNode();
		$end = $this->rel->getEndNode();
		$type = $this->rel->getType();
		$startId = $start->getId();
		$endId = $end->getId();

		if (!$start || !$start->hasId()) {
			throw new Exception('No relationship start node specified');
		}
		if (!$end || !$end->hasId()) {
			throw new Exception('No relationship end node specified');
		} else if (!$type) {
			throw new Exception('No relationship type specified');
		}

		$properties = $this->rel->getProperties();

		$statement = "MATCH (n),(o) ".
					 "WHERE ID(n)={$startId} AND ID(o)={$endId} ".
					 "CREATE (n)-[r:{$type} { ";

		foreach ($properties as $key => $property) {
			if (is_string($property))
				$statement .= "{$key}: '{$property}', ";
			else
				$statement .= "{$key}: {$property}, ";
		}

		// cypher statement to be run
		$statement = substr($statement, 0, -2);
		$statement.=' }]->(o) RETURN r';

		// organize data for 4.0+ API format
		$statements = array('statement' => $statement);
		$data = array('statements' => array($statements));

		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$start = $this->rel->getStartNode();
		if (!$start || !$start->hasId()) {
			throw new Exception('No relationship start node specified');
		}
		$host = $this->rel->getClient()->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to create relationship', $code, $headers, $data);
		}

		$relId = $data['data'][0][0]['metadata']['id'];

		$this->rel->setId($relId);
		$this->getEntityCache()->setCachedEntity($this->rel);
		return true;
	}
}

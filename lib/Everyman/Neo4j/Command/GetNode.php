<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Exception,
	Everyman\Neo4j\Node;

use Illuminate\Support\Facades\Log;

/**
 * Get and populate a node
 */
class GetNode extends Command
{
	protected $node = null;

	/**
	 * Set the node to drive the command
	 *
	 * @param Client $client
	 * @param Node $node
	 */
	public function __construct(Client $client, Node $node)
	{
		parent::__construct($client);
		$this->node = $node;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$nodeId = $this->node->getId();
		$statement = "MATCH (n) WHERE id(n)={$nodeId} RETURN n, n.label";
		$statements = array('statement' => $statement);

		// 4.0+ formatting
		$data = array('statements' => array($statements));
		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->node->hasId()) {
			throw new Exception('No node id specified');
		}
		$host = $this->client->getTransport()->getHost();
		return "/db/neo4j/tx/commit";

	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$this->node = $this->getEntityMapper()->populateNode($this->node, $data);
			$this->getEntityCache()->setCachedEntity($this->node);
			return true;
		} else {
			$this->throwException('Unable to retrieve node', $code, $headers, $data);
		}
	}
}

<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Exception,
	Everyman\Neo4j\PathFinder,
	Everyman\Neo4j\Path;

use Illuminate\Support\Facades\Log;

/**
 * Find paths from one node to another
 */
class GetPaths extends Command
{
	protected $finder = null;

	/**
	 * Set the parameters to search
	 *
	 * @param Client     $client
	 * @param PathFinder $finder
	 */
	public function __construct(Client $client, PathFinder $finder)
	{
		parent::__construct($client);

		$this->finder = $finder;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$startId = $this->finder->getStartNode()->getId();
		$endId = $this->finder->getEndNode()->getId();

		$statement = "MATCH path=(n)--(o) WHERE id(n)={$startId} AND id(o)={$endId} RETURN path";

		$statements = array('statement' => $statement);
		$data = array('statements' => array($statements));

		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$start = $this->finder->getStartNode();
		if (!$start || !$start->hasId()) {
			throw new Exception('No start node id specified');
		}
		$host = $this->client->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return integer on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to retrieve paths', $code, $headers, $data);
		}

		$paths = array();
		foreach ($data as $pathData) {
			$paths[] = $this->getEntityMapper()->populatePath(new Path($this->client), $pathData);
		}
		return $paths;
	}
}

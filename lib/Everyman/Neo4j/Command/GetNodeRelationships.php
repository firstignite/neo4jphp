<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Exception,
	Everyman\Neo4j\Relationship,
	Everyman\Neo4j\Node;

use Illuminate\Support\Facades\Log;

/**
 * Find relationships on a node
 */
class GetNodeRelationships extends Command
{
	protected $node  = null;
	protected $types = null;
	protected $dir   = null;

	/**
	 * Set the parameters to search
	 *
	 * @param Client $client
	 * @param Node   $node
	 * @param mixed  $types a string or array of strings
	 * @param string $dir
	 */
	public function __construct(Client $client, Node $node, $types=array(), $dir=null)
	{
		parent::__construct($client);

		if (empty($dir)) {
			$dir = Relationship::DirectionAll;
		}
		if (empty($types)) {
			$types = array();
		} else if (!is_array($types)) {
			$types = array($types);
		}

		$this->node = $node;
		$this->dir = $dir;
		$this->types = $types;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$nodeId = $this->node->getId();
		$relTypes = "'".implode(', ', $this->types)."'";

		// construct our query based on relationship's directionality
		// the id() calls at the end are for the start and end node
		// in the curl request's response formatting in Curl.php
		if ($this->dir == 'out') {
			$statement = "MATCH (n)-[r]->(o) WHERE id(n) = {$nodeId} AND type(r) IN [{$relTypes}] RETURN r, type(r), id(n), id(o)";
		} elseif ($this->dir == 'in') {
			$statement = "MATCH (n)<-[r]-(o) WHERE id(n) = {$nodeId} AND type(r) IN [{$relTypes}] RETURN r, type(r), id(o), id(n)";
		} elseif ($this->dir == 'all') {
			$statement = "MATCH (n)-[r]-(o) WHERE id(n) = {$nodeId} AND type(r) IN [{$relTypes}] RETURN r, type(r), id(n), id(o)";
		}
		else {
			throw new Exception('Unaccounted relationship direction in GetNodeRelationships: ' . $this->dir);
		}

		$statements = array('statement' => $statement);

		// 4.0+ formatting
		$data = array('statements' => array($statements));
		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->node->hasId()) {
			throw new Exception('No node id specified');
		}

		$host = $this->client->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return integer on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$rels = array();
			foreach ($data as $relData) {
				$rels[] = $this->getEntityMapper()->makeRelationship($relData);
			}
			return $rels;
		} else {
			$this->throwException('Unable to retrieve node relationships', $code, $headers, $data);
		}
	}
}

<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Exception,
	Everyman\Neo4j\EntityMapper,
	Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Cypher\Query,
	Everyman\Neo4j\Query\ResultSet;

use Illuminate\Support\Facades\Log;

/**
 * Perform a list of queries using the Cypher query language and return the results
 */
class ExecuteBulkCypherQuery extends Command
{
	protected $statements = null;

	/**
	 * Set the query to execute
	 *
	 * @param Client $client
	 * @param Query $query
	 */
	public function __construct(Client $client, Array $statements)
	{
		parent::__construct($client);
		$this->statements = $statements;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$data = [];
		foreach ($this->statements as $statement) {
			$data[] = array('statement' => $statement);
			// if we had params
			// $data[] = array('statement' => $statement, 'parameters' => $parameters)
		}

		// 4.0+ formatting
		$data = array('statements' => $data);
		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$url = $this->client->hasCapability(Client::CapabilityCypher);
		if (!$url) {
			throw new Exception('Cypher unavailable');
		}

		$host = $this->client->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return integer on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to execute query', $code, $headers, $data);
		}
		return new ResultSet($this->client, $data);
	}
}

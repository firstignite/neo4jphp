<?php
namespace Everyman\Neo4j\Command;

use Everyman\Neo4j\Command,
	Everyman\Neo4j\Client,
	Everyman\Neo4j\Exception,
	Everyman\Neo4j\Relationship,
	Everyman\Neo4j\Node;

/**
 * Get and populate a relationship
 */
class GetRelationship extends Command
{
	protected $rel = null;

	/**
	 * Set the relationship to drive the command
	 *
	 * @param Client $client
	 * @param Relationship $rel
	 */
	public function __construct(Client $client, Relationship $rel)
	{
		parent::__construct($client);
		$this->rel = $rel;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		$relId = $this->rel->getId();
		$statement = "MATCH (n)-[r]-(o) WHERE id(r)={$relId} RETURN r, type(r), id(startnode(r)), id(endnode(r))";

		$statements = array('statement' => $statement);
		$data = array('statements' => array($statements));

		return $data;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'post';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->rel->hasId()) {
			throw new Exception('No relationship id specified');
		}
		$host = $this->client->getTransport()->getHost();
		return "/db/neo4j/tx/commit";
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$this->rel = $this->getEntityMapper()->populateRelationship($this->rel, $data);
			$this->getEntityCache()->setCachedEntity($this->rel);
			return true;
		} else {
			$this->throwException('Unable to retrieve relationship', $code, $headers, $data);
		}
	}
}

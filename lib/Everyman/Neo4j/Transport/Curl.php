<?php
namespace Everyman\Neo4j\Transport;

use Everyman\Neo4j\Transport as BaseTransport,
	Everyman\Neo4j\Version,
	Everyman\Neo4j\Exception,
	Illuminate\Support\Facades\Log;

/**
 * Class for communicating with an HTTP JSON endpoint
 */
class Curl extends BaseTransport
{
	protected $handle = null;

	/**
	 * @inherit
	 */
	public function __construct($host='localhost', $port=7474)
	{
		if (! function_exists('curl_init')) {
			throw new Exception('cUrl extension not enabled/installed');
		}

		parent::__construct($host, $port);
	}

	/**
	 * Make sure the curl handle closes when we are done with the Transport
	 */
	public function __destruct()
	{
		if ($this->handle) {
			curl_close($this->handle);
		}
	}

	/**
	 * @inherit
	 */
	public function makeRequest($method, $path, $data=array())
	{
		$url = $this->getEndpoint().$path;

		$options = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,
			CURLOPT_HTTPHEADER => array(
				'Accept: application/json;stream=true',
				'Content-type: application/json',
				'User-Agent: '.Version::userAgent(),
				'X-Stream: true'
			),
			CURLOPT_CUSTOMREQUEST => self::GET,
			CURLOPT_POST => false,
			CURLOPT_POSTFIELDS => null,
			CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
		);

		if ($this->username && $this->password) {
			$options[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
			$options[CURLOPT_USERPWD] = $this->username.':'.$this->password;
		}

		switch ($method) {
			case self::DELETE:
				$options[CURLOPT_CUSTOMREQUEST] = self::DELETE;
				break;

			case self::POST:
			case self::PUT:
				$dataString = $this->encodeData($data);
				$options[CURLOPT_CUSTOMREQUEST] = $method;
				$options[CURLOPT_POSTFIELDS] = $dataString;
				$options[CURLOPT_HTTPHEADER][] = 'Content-Length: '.strlen($dataString);

				if (self::POST == $method) {
					$options[CURLOPT_POST] = true;
				}
				break;
		}

		$ch = $this->getHandle();
		curl_setopt_array($ch, $options);

		$response = curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

		if ($response === false) {
			throw new Exception("Can't open connection to ".$url);
		}

		if (!$code) {
			$code = 500;
			$headerSize = 0;
			$response = json_encode(array("error"=>curl_error($ch).' ['.curl_errno($ch).']'));
		}

		$bodyString = substr($response, $headerSize);
		$bodyData = json_decode($bodyString, true);

		$headerString = substr($response, 0, $headerSize);
		$headers = explode("\r\n", $headerString);
		foreach ($headers as $i => $header) {
			unset($headers[$i]);
			$parts = explode(':', $header);
			if (isset($parts[1])) {
				$name = trim(array_shift($parts));
				$value = join(':', $parts);
				$headers[$name] = $value;
			}
		}

		$bodyData = $this->translateResults($bodyData);

		return array(
			'code' => $code,
			'headers' => $headers,
			'data' => $bodyData,
		);
	}

	/**
	 * Translate the 4.2 API response to the 3.5 format
	 *
	 * @param array  $result
	 * @return array formatted for 3.5 response
	 */
	public function translateResults($result)
	{
		if (is_array($result) && isset($result['errors'][0])) {
			$errors = $result['errors'][0];
			throw new \ValueError(json_encode(['code' => $errors['code'], 'message' => $errors['message']]));
		}

		if (is_array($result) && array_key_exists('results', $result)) {

			// getNode
			if (isset($result['results'][0]['columns'][1]) && $result['results'][0]['columns'][1] == 'n.label') {
				return $this->translateNodeResults($result);
			}
			// getRelationship, getNodeRelationships
			if (isset($result['results'][0]['columns'][1]) && $result['results'][0]['columns'][1] == 'type(r)') {
				return $this->translateEdgeResults($result);
			}
			// getPaths
			if (isset($result['results'][0]['columns'][0]) && $result['results'][0]['columns'][0] == 'path') {
				return $this->translatePathResults($result);
			}

			$result['columns'] = $result['results'][0]['columns'];

			// execution path for general results like ExecuteCypherQuery
			if (isset($result['results'][0]['data'][0]['meta'][0])) {
				if (!is_null($result['results'][0]['data'][0]['meta'][0])) {
					foreach ($result['results'] as $ri => $res) {
						foreach ($res['data'] as $index => $row) {
							foreach ($row['row'] as $rowIndex => $rowNode) {
								$result['results'][$ri]['data'][$index][$rowIndex]['data'] = $rowNode;
							}
							foreach ($row['meta'] as $metaIndex => $rowMeta) {
								$result['results'][$ri]['data'][$index][$metaIndex]['metadata'] = $rowMeta;
								if (isset($result['columns'][$metaIndex]))
									$result['results'][$ri]['data'][$index][$metaIndex]['metadata']['labels'] = array($result['columns'][$metaIndex]);
								if (isset($row['meta'][$metaIndex]['id']))
									$result['results'][$ri]['data'][$index][$metaIndex]['self'] = $this->getEndpoint().'/'.$row['meta'][$metaIndex]['id'];
							}
							unset($result['results'][$ri]['data'][$index]['row']);
							unset($result['results'][$ri]['data'][$index]['meta']);
						}
					}
				}
			}

			// execution path for raw data results (count(*))
			else {
				foreach ($result['results'][0]['data'] as $index => $row) {
					foreach ($row['row'] as $rowIndex => $rowNode) {
						$result['results'][0]['data'][$index][$rowIndex] = $rowNode;
					}
					unset($result['results'][0]['data'][$index]['row']);
				}
			}

			$result['data'] = $result['results'][0]['data'];
			unset($result['results']);
		}
		return $result;
	}

	/**
     * Handle Node commands (getNode)
     *
     * @param  array  $result
     * @return formatted for 3.5 response
     */
	public function translateNodeResults($result)
	{
		if ($result['results'][0]['data'] == []) {
			return array();
		}

		$data = [];

		foreach($result['results'][0]['data'] as $index => $node) {
			$data = array(
				'data' => $node['row'][0],
				'metadata' => $node['meta'][0],
				'self' => $this->getEndpoint().'/'.$node['meta'][0]['id'],
				'type' => $node['meta'][0]['type']
			);
		}

		return $data;
	}

	/**
     * Handle Relationship commands (getRelationship, getNodeRelationships)
     *
     * @param  array  $result
     * @return formatted for 3.5 response
     */
	public function translateEdgeResults($result)
	{
		if ($result['results'][0]['data'] == []) {
			return array();
		}

		$data = [];

		// getRelationship
		if ($result['results'][0]['columns'][2] == 'id(startnode(r))') {
			foreach($result['results'][0]['data'] as $index => $edge) {
				$edge['meta'][0]['type'] = $edge['row'][1];
				$data = array(
					'data' => $edge['row'][0],
					'metadata' => $edge['meta'][0],
					'self' => $this->getEndpoint().'/'.$edge['meta'][0]['id'],
					'start' => $this->getEndpoint().'/'.$edge['row'][2],
					'end' => $this->getEndpoint().'/'.$edge['row'][3],
					'type' => $edge['row'][1]
				);
			}
		}
		// getNodeRelationships
		else {
			foreach($result['results'][0]['data'] as $index => $edge) {
				$edge['meta'][0]['type'] = $edge['row'][1];
				$data[$index] = array(
					'data' => $edge['row'][0],
					'metadata' => $edge['meta'][0],
					'self' => $this->getEndpoint().'/'.$edge['meta'][0]['id'],
					'start' => $this->getEndpoint().'/'.$edge['row'][2],
					'end' => $this->getEndpoint().'/'.$edge['row'][3],
					'type' => $edge['row'][1]
				);
			}
		}

		return $data;
	}

	/**
     * Handle Path commands (getPath)
     *
     * @param  array  $result
     * @return formatted for 3.5 response
     */
	public function translatePathResults($result)
	{
		if ($result['results'][0]['data'] == []) {
			return array();
		}

		$data = [];
		$relationshipPrefix = $this->getEndpoint().'/db/data/relationship/';
		$nodePrefix = $this->getEndpoint().'/db/data/node/';

		foreach($result['results'][0]['data'] as $index => $path) {
			$data[$index] = array(
				'relationships' => array($relationshipPrefix.$path['meta'][0][1]['id']),
				'nodes' => array($nodePrefix.$path['meta'][0][0]['id'], $nodePrefix.$path['meta'][0][2]['id']),
				'length' => 1,
				'start' => $nodePrefix.$path['meta'][0][0]['id'],
				'end' => $nodePrefix.$path['meta'][0][2]['id'],
				'directions' => array('<-')
			);
		}

		return $data;
	}

	/**
	 * Get the cURL handle
	 *
	 * @return resource cURL handle
	 */
	protected function getHandle()
	{
		if (!$this->handle) {
			$this->handle = curl_init();
		}
		return $this->handle;
	}
}
